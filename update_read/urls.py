from django.urls import path

from .views import *

app_name = "update_read"

urlpatterns = [
    path('update', update_view),
    path('read/<int:npm>', read_view),
]
