from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Mahasiswa
import json

@csrf_exempt
def update_view(request):
    json_data = json.loads(request.body)
    npm = json_data['npm']
    nama = json_data['nama']

    mhs = Mahasiswa.objects.filter(npm=npm).first()

    if mhs is None:
        new_mhs = Mahasiswa.objects.create(npm=npm, nama=nama)
        new_mhs.save()
        return JsonResponse({'status':'OK'})
    else:
        # return status error existing data
        return JsonResponse({
            'status':'ERROR',
            'message':'Data dengan npm tersebut sudah terdaftar'
            })


def read_view(request, npm, trx_id=None):
    print(f"npm: {npm}")
    mhs = Mahasiswa.objects.filter(npm=npm).first()
    if mhs:
        return JsonResponse({
            'status':'OK',
            'npm':mhs.npm,
            'nama':mhs.nama
            })
    else:
        return JsonResponse({
            'status':'ERROR',
            'message':'Data dengan npm tersebut tidak ditemukan'
            })